### Mutated My Taxes: MMT for Austrians, Republicans, conservatives, dummies et al ###
© Copyright reserved

.

Dedications: to Ron Paul & Ben Bernanke, to Wray, Mitchell & Mosler, to all busy politicians who are just humans and often unfairly hated, to Judas, Caroline and Jesus. To Galileo, Columbus, Copernicus, Comenius, Newton, Einstein and Darwin. And, above all, God, by any name or unnamed, in Us all.

.

.

Now you successfully passed through the filter of the dedications paragraph and this means you can successfully stomach complex and controversial concepts including God and love for Judas or, God forbid, even for politicians. Congratulations. The remaining two or three of you still reading are therefore well intellectually and morally set up for this learning journey. I will try to not waste your time, as I did not with the brigade of the people who didn't make it past the filter - God bless them nonetheless.

Allow me to briefly set the scene with what we all - Republicans, populists, Austrians, conservatives and dummies - hopefully agree on.

The will of the People on the federal level of the United States is represented by the Congress. The Congress has the plenary power to issue the United States currency, the Dollar. The Congress, the will of the People, also passes laws and spends dollars for the benefit of the People of the United States, for example, purchasing from people their time and other resources to achieve defense of our United States in a time of war.

The Congress under the will of the People is also allowed and empowered to impose federal taxes. Taxes could be paid to the Congress, the People's own, federal self-government, in various units of account: grams of gold (per amount of wealth created, per fine issued, per government's resource leased), or any other way our self-government deems useful. With this gold or other "money units" the Congress can recruit soldiers, teachers, can re-purchase private land and buy asphalt and machines to build roads. If one or more of us, the People, or one of the corporations we let operate in our nation, doesn't pay the taxes, the rest of us the People will put you to jail or prevent your corporation from trading with us, or Us, in the US. And that's why people and companies pay the tax thus imposed.

Gold is money (notice across the world you never pay sales tax, VAT, GST, MwST and so on when you buy gold, confirming its moneyness) and it's great money and a great store of value because of its scarcity. It also has some limitations. If we the People in the Congress agree to use gold as our currency, we become this currency's users. We have to get the gold from someone who already has it. When we want to use it it can be cumbersome to split. Even the smallest gold coin is worth quite a lot, but we can use other metals with intrinsic value. But even that doesn't cover all use cases, so through history we invented substitute pieces of paper, IOUs ("I-owe-you") that just *note* that there is goldmoney in the bank. We have learned to trust some banks and accept their private bank notes as if the notes were the (gold)money itself. And we even accept private personal checks which are just bank notes without the amount filled in, and the money "issuer", Mr. Goldstein, or you or I then become a private mini-bank for the purpose of that particular transaction. Pretty cool! We humans are great at trusting each other and when we don't we are good at working around it: we use escrow and release the goods to Joe Goldstein only after his personal check clears. It clears either into private banknotes or into the underlying gold, the ultimate money. (Or so it might have briefly used to work.)

So the Congress imposed this tax on you in the 2nd paragraph above, to be paid in gold, and you are now standing at the teller window at the Bank of Solomon Bros holding Mr. Goldstein's check you got for selling him the cow. And you have no choice but to pay the tax because jailtime is not worth a rebellion against your fellow People in Congress. Hold that thought a little longer.

History teaches us, as is documented across multiple nations (1), that one thing always happens. And will always happen, this time and the next time are no different, such is the human nature, you won't change it for long even if you became a successful totalitarian or a religious leader. The risk of a check or a note conversion boucing is perceived by the economy participants as very low at any given time, relative to the total amount of transactions. Very quickly people stop worrying about the underlying gold and are happy to transact with the notes, because they know with 99.99999999% confidence that the notes will be accepted by the counterparty in the next transaction. While that confidence level might not be good enough for you, it is for people, for humans, and it always will be.
The thing that always happens with human societies is that metalism is happily forgotten and societies operate with *receipts* as money. The myth of convertibility into a scarce resource and the fact of day-to-day convertibility into real resources are enough to keep the system going. The convenience outweighs the risk of conversion default. 

Do you disagree or have an odd feeling reading the above? 99.99999999%? I hope you feel odd, because we just conflated two systems of money, and understanding the difference is the critical ingredient that is needed to understanding money, government spending, the theory of money, the theory of modern money, Modern Money Theory. This ingredient, the understanding that there are multiple money systems and being able to de-conflate between them, is missing and I believe will continue to be missing from most people's minds. Hopefully will not be from yours. This ingredient is what will hopefully allow you to understand why not "Magic Money Tree" but rather "Mutated My Taxes" might be a better one-liner representation of what MMT (Modern Money Theory, Modern Monetary Theory, Theory of Modern Money) is when applied to the finances of a sovereign currency issuer, such as U.S. Congress.

You are standing in the branch of your trusted bank, Solomon Bros, with a "Pay bearer 1234.50 Solomonbrosgolddollars, for cow" note/check/i-owe-you-certificate from your trusted transaction partner Mr. "the mini-bank" Goldstein, and the thought of jail time is hanging over your head because you owe 4 ounces of gold to your fellow People as represented by the Congress.

So how do you pay 4 oz of gold to Congress when what you have is 5000 of Solomonbrosgolddollars in your existing Solomon Bros account and also in your hand you have a custom  minibanknote/IOU worth 1234.50 Goldsteinsolomonbrosgolddollars issued by the minibank Goldstein that promises to convert to 1234.50 Solomonbrosgolddollars when presented to the teller window of Solomon Bros?

Let's clear Goldstein's dollars first into Solomons' dollars and deal with the Congressional inconvenience later.
You give the venerable Goldstein's check to the teller and it doesn't bounce. "Goldstein is a trustworthy guy - maybe he should open a bank, to give some competition to the Bros!" flows through your mind. Your account balance at Solomon Bros now says "6234.50 golddollars", but since you've never taken actual gold out, you think about it as "6234.50 Solomonbrosgolddollars" out of abundance of caution as you've always been paranoid about being the guy that draws the 0.00000001% ticket one day, instead of gold.

And what are "golddollars" anyway? The Congress wants ounces of gold and not some dollars!
Turns out that counting in whole ounces was inconvenient to bankers and their customers so the Association of Gold Bankers (AGB) agreed to create a new unit for their gold accounting purposes. The exchange rate is 1000 golddollars for 1 oz of gold. This means that if the Solomons don't default on their IOU to you and the conversion from your deposit of 6234.50 solomongolddollars (showing as "AGB golddollars" or simply "AGB$" on your statement) to real gold doesn't fail, you have about 6.2 ounces of gold. Enough to pay your tax to Congress and a bit more. 
You ask the teller to convert AGB$4000 solomongolddollars to gold, exhale a sigh of relief as the teller emerges from the vault with 4 oz of gold and you take the gold to the Federal Treasury office which conveniently happens to be two doors down the street. You go to bed in you home next to your wife instead of to jail and next to smelly fraudsters with names like Ponzi and Madon. The Congress takes the gold and gives it to your neighbour Sam who is a soldier leaving town tomorrow to fend off a military attack on the United States by a neighboring nation that fancies exclusive ownership of US gold mines.

The Government's (Central) Bank Note and Reserve Currency is Born

The next day you wake up and there's a letter from the Congressional Tax Office in your mailbox. "By way of law passed two weeks ago in People's Congress, due to continued challenges and inconveniences with accepting, storing and distributing gold related to day-to-day government transactions, from today all tax obligations are to be paid in Reservedollars and not gold. We are establishing a new Federal Gold Currency, United States Goldreservedollars, or USDR, R$, or $, the issuance of which and the respective gold stock backing it is to be managed by a new Congressional agency, the Federal Goldreserve, or Fed for short".
It continues: "The Fed will exchange United States Goldreservedollars to gold on demand, at an exchange rate of R$1000 to 1 ounce of gold. For your accounting convenience this is in line with the custom previously established by the Association of Gold Bankers for exchange of their private currency, the AGB$, into gold at participating private banks."

Years go by, hundreds of private banks spring up like mushrooms and then go bust again from issuing more AGB golddollar notes than gold they had in their vaults and clients then wanting to convert their IOUs into the promised gold. Not all private banks have the best reputation and trust in each other, so since all of them already have Goldreservedollar accounts at the Fed (used to forward your 4-oz-worth of gold tax obligation to the government), instead of creating a complicated brittle decentralized system of peer-to-peer network of (dis)trust and clearing between each minibank, they start clearing between each other at the Fed in US Goldreservedollars. For this they move some of the gold from their private vaults to the Fed's vault, receiving USDR dollar notes (or simply deposit entries on their account ledger at the Fed) in exchange. Thanks to this surplus private, non-tax gold in the Federal Reserve System the private banks clear their customer's interbank transactions, without any need for Washington Savings Bank management to worry about Solomon's AGB$-denominated notes having no gold behind them and bouncing on an attempted conversion to gold, or the alternative of needing to haul actual gold back and forth at the end of each day between private bank vaults, in the firing line of various Robinhoods.

Everyone is happy. Austrians are drinking schnapps and yodeling. But the economy is not growing that much because when an enterpreneur has a great idea and needs to borrow some capital to change the world, the only money the Solomon's can lend him for his project is the money/gold that previous depositors put into the vault. And these depositors are typically not friends of risky ventures. They prefer to preserve wealth rather than risking it to build more of it, or to lose it all, so the availability of credit is tighter than the eye of the needle.

The people in Congress acknowledge this constraint that inhibits the potential of their nation.
Also the war that your neighbour Sam went to goes on forever.

Someone at the Fed (Or was it at the ABA? Or was it someone who was first at the ABA and now at the Fed, or vice versa?) has a clever revolutionary idea on how to unleash much of the hidden economic potential by way of making credit much more widely accessible while not dramatically increasing the risk in the monetary/financial system: 

"Since for every AGBgolddollar IOU asset lent out to the borrower the borrower enters into an obligation to repay it (sometimes securitized by a real asset such as a mortgage over immovable property) and on average only 1 in every 10 of those ventures blow up spectacularly, while the interest on the other 9 almost pays for the 10th's principal (and for the operation of the private bank) anyway, let's do this:"

"Let's give the private banks - who are good at assessing creditworthiness - a licence to issue money to borrowers, denominated in Federal US Goldreservedollars, and for every 10 dollars issued have them deposit only 1 US goldreservedollar worth of physical gold, or a USgolddollar account deposit, as capital requirement."

If something goes wrong - as it does every now and then when the credit teams at the banks get a bit ahead of themselves, competing with other banks' credit teams for a good borrower and his obligation and future interest payments - the people's Fed promises to sort out the situation and step in and bail it all out with its own reservedollars, which are (still) equivalent with physical gold.

"Nein!" shout the Austrians, their yodeling interrupted. "Yay!" shout the Solomons and then the the rest of the world and papa Elon Musk, aunt Cathie and Gottlieb Daimler, while Peter Thiel takes a moment to consider the deep moral implications of this proposal, from his conservative position.

This is where the Austrian world story ends, for the first time, on private-bank-fiat level. Where the fantasy is shattered by human nature, and endeavor. The real world of fiat money enters the scene, again. In this world most entities are currency users, but there's one entity, almost a deity, the currency issuer, who will soon have certain special - almost divine - options, and who fortunately happens to be us, the People, in Congress.

Sam has now been at war for over a decade and meanwhile became an uncle. Over those 10 years, the government needed more tanks and Sams than initially expected and than taxes could pay for. In the first couple of years it would borrow private gold and issue goldreservedollar-denominated bond note coupons to kind patriotic creditors, with a bit of interest attached to them to sweeten the deal and not rely solely on patriotism. The government enjoyed great trust from these creditors, the sort of creditors from three paragraphs ago who are not friends of risky ventures: a great trust at least relatively to other entities competing for their money. The trust was so great that these bonds became a benchmark of what it means to have a "risk free" yet interest-paying financial asset.

"It's like a savings account!" rejoiced the creditors, "but not with the dodgy Solomon's with questionable vaults, it's with the megabank of the U.S., the People of the United States themselves!" As the years dragged on and the coupons started to be paid back out to gold and the vault at the Fed stopped being as shiny a place as it once used to be (but still shinier than some other nations' vaults) an accountant at the Fed realised that the golddollar nominal value of all the bonds outcounted the golddollar value of the gold in the vault by a factor of 10, which she thought could result in a bit of a sketchy situation, for the People of the United States.

The spectre of convertibility Default now loomed even over the Feds vaults.

To avoid the embarassment of conversion vultures descending at the vaults and the hard feelings of inequality and unfairness of some later vultures (I mean creditors and fellow note-holding citizens) stemming from being surprised that the IOUs in their wallets didn't convert to gold anymore, since the government of the People of the United States had a relatively pretty good goodwill and creditibility [sic] compared to other nations due to the stability of the nation's political system (as fronted by the non-maniac Congress) and the size of the real economy, it was agreed that, for example from 01971, goldreservedollars are becoming gold-less reservedollars. From now on, for one reservedollar you'll only get another reservedollar, no gold.

Problem solved. Everyone got screwed equally, so no one complained. Few Austrians yodeled. The Pope said: "Fiat Dollar!" Samuelson said something along the lines that good old fashioned religion and aiming for a balanced "budget" as if there was still a real-scarce-resource constraint will be good enough. (2)
And it was, as it always is with productive economies governed by non-maniac governments. Because it continued working for the 99.999999999999999% of transactions. For most people most of the time, the value was and is in the concept and the universal acceptance (driven by Congress imposing taxes in reservedollars), rather than the intrinsic (non)value.

This is how real governments work, as opposed to the 3rd world proto-government example from the introductory paragraphs above. 
This is where the Austrian story ends, for the second time, on Fed-fiat level.
And this is where MMT, describing a financial system with a sovereign currency issuer (rather than user) begins.

The diagram below illustrates the system's various evolutionary stages described above, as represented by the private side and government side balance sheets, with commercial banks at the boundary between the private and government subsystems.
http://mutatedmytaxes.com/assets/01systemdiagram02.jpg

.

TBC
====

.

.

----

.

.

$USDR 
$USDP

An interesting thing happened in 01971. We the People in Congress chose to emancipate our money from gold. We suffraged and defaulted on the dollar's convertibility to gold - to us and to our creditors.
We became a true sovereign Currency Issuer (not currency user) to the absolute extent possible. A new paradigm began where we are neither a user of gold, neither a proxy user of gold through being an issuer of Goldreservedollars. We the People are, and for the past 50 years have been, a Sovereign Issuer of Dollars. Not a currency user, a currency issuer.

Now this brings a couple of problems.



Footnotes:

1) L. Randall Wray - Modern Money Theory for Beginners https://www.youtube.com/watch?v=E5JTn7GS4oA (uploaded Apr 26, 2018. St. Francis College Youtube channel)

2) Paul Samuelson: The balanced budget myth (in Buchanan interview) https://www.youtube.com/watch?v=4_pasHodJ-8 1:27 onward (uploaded Mar 2, 2014. Wonkmonk Youtube channel)



----

What would happen if every single depositor in every private bank - that is licenced to participate in the Federal Reserve System and issue credit - demanded to convert their deposit at the private bank to cash or reserves? The Fed would print the bank notes, or, in other words/alternatively, would by way of a few keystrokes on a computer mark up the private banks' reserve accounts at the Fed to the equal value of the reserves demand and thus easily satisfy the demand. 

**This argument establishes the equality (identity) of Fed reserves vs. private bank-issued money/credit, as to the effect on money supply.**

How would this work out accounting-wise? These new tens of trillions of reserve dollars (which are now liabilities on the Fed's balance sheet, and assets on the private banks' balance sheet) now caused an imbalance on the balance sheets. The Fed can't just gift the money to the private banks and get nothing for it, while the private banks would keep collecting interest on the trillions dollars worth of private citizens' and corporations' loan repayment promises they still hold on the asset side of their balance sheets. That would be the ultimate bailout scam. No, the Fed, as it has always done, will require the private banks to transfer also the ownership of the loan assets from their balance sheets to the Fed's balance sheet, as collateral to the newly issued reserves. It is a nationalisation of private loans and the credit money, but the the original holders of the loan assets make the transfer voluntarily to avoid the run bankrupting them..

The cost, other than incentivizing/introducing moral hazard into the system, is Fed/new spending footing the LTV difference between the size of the deposit and the real value of the collateral (the "toxic delta"), causing some but not huge inflation pressure (unless the amounts of the written off value are extreme).

After this operation the banks would effectively become pure cash vaults where deposits would map 1:1 to the cash in the vault. At that singular point in time, the private banking system would manage no single dollar of private credit whatsoever.

This fact of the identity of reserves on the Fed side and "latent reserves" on the private side has a critical implication on understanding inflation and the traditional Austrian (mis)understanding of the money supply. If the Fed can effortlessly print dollars for any private loan in existence, we can identify the entirety of the M3 supply including MZM as "latent reserves", or for simplicity can just call it reserves, due to such identity.

The money is already in the system.

The inflation pressure has already played out before the time the Fed carried out the "monetization operation" into reservedollars.

**The Fed simply converted Solomondollar latentreservedollar deposits to USdollar deposits**, letting the money (latent reserves) and the promise-to-repay jump from the privatedollar system to the public/USdollar system, and with it it took over the management of collateral and the management of the extinguishing of the loan over time and with that again extuingishing-burning-removingfromthesystem the respective reservedollars.

What this means is that when a central bank issues reserves in a quantitative easing operation buying government bonds, or in a private debt bail-in operation (taking the loan as collateral onto its balance sheet as an asset - of varying degree of toxicity or "assetness") this operation by the Fed has ZERO INFLATIONARY EFFECT as the government spending or private bank credit issuance ALREADY HAPPENED A PRIORI, IN THE PAST (be it a couple of weeks in the case of Coronavirus relief, or years in case of most private credit such as mortgage-backed securities).

So Austrians can now go to bed and sleep well, knowing that the inflation is alredy baked into the monetary system before the Fed starts increasing the size of its balance sheet. However what's not baked into the system is future government spending, or strictness of private lending standards (i.e. risk management the size of the new credit vs. the collateral valuation going out of sync e.g. through fraud). This is where we the People can have a political match in the market of spending ideas and ideology, the outcome of which is to be ultimately implemented by our People's Congress and its spending and taxation, or fiscalmonetary policy, as implemented - in MMT parlance - by the consolidated government agency of the Treasury and the Fed, as an extended arm of Congress.


------

##### Rephrasing of the above in letter 1:

I'm drafting a short paper with a working title "Money Theory: MMT for Austrians, Republicans, Chicagoers, conservatives, populists and dummies" where I'm attempting to induce a Gestalt shift in the minds of people from the above groups and get them to understand MMT (and they or I can then happily continue to advocate for a small government policy after that, e.g. conducing a potential to abolish the federal income tax for the majority of people, thanks to small inflationary pressure from small gov spending).

While thinking through my arguments against the fear of Fed asset purchases (both public bonds in QE as well as private stuff)  and arguing that the resulting creation of reserves is not a money supply/inflationary concern, I thought of an ad-absurdum example of a Fed asset purchase and now wondering if you thought about the scenario too, and might have some terminology or literature of your own on the topic that you could point me to?

I'm calling the concept "latent reserves", as this scenario IMHO eatablishes an identity of reserves/HPM (I sometimes call them USDR) and private system deposits (USDP - p for "private dollars" or "promised HPM dollars", disambiguating from USDR on the Fed side):

Imagine every deposit holder at every private bank all at once decide to withdraw their deposit as cash (or transfer every single deposit to a new shiny commercial bank, Bank of Shinea). In this  run (or lack of reserves for clearing) situation, the Fed will effortlessly step in and print all notes needed to satisfy the demand for the new HPM (or will mark up the banks' reserve accounts by way of a few computer keystrokes and the clearing will easily proceed). This fact of near effortlessness IMHO eatablishes the effective identity of private credit/deposits on the private side with HPM/reserves on the CB side. Would you agree?
Of course Fed doesn't just print the HPM, it also forces the commercial banks to transfer the corresponding loan assets to Fed and the Fed puts these on the asset side of their balance sheet to balance out the new HPM.

In effect the Fed simply became responsible for the continued management of all private credit (and Fed is a private corporation after all) or "all money" and every private debtor will now repay each loan dollar to the Fed, which will also over time extinguish most of the newly created HPM (minus the value of the toxicity writeoffs/LTV mismatches). At this precise point in time, for a moment the two systems (private+Fed) just became one, all money is HPM(-backed) money. In the cash variant of the scenario, all private banks just became pure vault storage shops with 1:1 mapping between deposit account numbers and USD reserve notes deposited in vault. There is $0 dollars of private, non-HPM credit in the system, for a moment.

Have you thought about private bank credit/deposits in this "latent" or "potential reserves" way before? Under this thinking, the two money systems really become one system, "down to the money", one supply.
The Fed "monetization" of private debt being just another asset class swap (non-HPM to HPM), similar to the treasuries-to-reserves swap and "no new money printing" occurs/no new supply is created unlike what the fearful Austrians go on about (distracting themselves from inflation pressure contributors like fiscal policy, lax private lending standards causing LTV mismatch, and new money issued by gov for Treausry "debt" maintenance).
Could this "reservification-by-keystroke" argument give them no choice but make the mental leap?


##### Rephrasing in follow up letter 2:

If all money is HPM (reserves) and easily-HPM-able money (sum of deposits in commercial banks), if you agree with my example scenario, then, in the 2nd minute of the life of a monetary system,

a) you don't need government to spend first for a private buyer to be able to buy gov bonds, assuming the reserves for the settlement have come from Fed purchasing a private loan asset created just before - in the 1st minute of the system's life.
Government doesn't start spending until the 3rd minute.

b) while no new net financial assets have been created, there are already reserves in the system to get it going and "multiply", before government even started actual spending.

As I understand MMT, it says that gov needs to spend first. Austrians disagree with that. The example above brings the two theories together, no?
I don't consider the Fed asset purchase as government spending (it doesn't create any net financial assets after all). Do you agree?









----

Topics:

* I will try to not waste your time.
* We Austrians, Republicans, conservatives...
* Will children pay back the "debt"?
* Non-senscial words in currency-issuer's world: "debt", "budget" ["to budget in one's own monopoly currency" - makes no sense]
* Distrust in government
* More than describing a Magic Money Tree, MMT Mutated My Taxes - the way we understand the function of federal taxes.
* Gestalt shift
* 1971 (or was it 1973), vs conflating or equaling inflation of a bag or bubble of money supply (monetary base or broad money incl. dollar-denominated deposits created as credit by private banks) with CPI inflation and increasing dollar-denominated amount of our day-to-day living expenses (food, housing, transport, energy, holidays, education).
* Samuelson myth/good old fashioned religion statement
* Bernanke creature of Congress statment
* Less than 42 (75?) pages of Wittgenstein's Tractatus Logico-Philosophicus exactly 100 years ago 1921 https://en.wikipedia.org/wiki/Tractatus_Logico-Philosophicus
* Potential to abolish personal income for majority of population, without increasing inflation
* Inflation
* Gravity vs flying a plane analogy - altitute being government spending relative to aggregate demand (add an Icarus analogy/warning?), the pitch angle being the direction of inflation change?
* Small government.
* Balance sheets and double-entry accounting
* Settlement of interbank transactions and tax obligations in $USDR.
* Burning tax dollars and printing shiny new $USDR dollars vs recycling tax dollars - no difference.
* Reserve dollars $USDR and private dollars promissed to be converted to reserve dollars on demand, $USDP. Endogenous money.
* Currency user vs currency issuer
* Automatic stabilizers (Abba Lerner)/Federal Inflation Safety, Targetting & Regulation Commission ("FISTR" :) ) - same independent role as Fed, could sit within Fed but chairman of House of Representatives would be the chairman of FISTR.
* Deficit happens ex-post (it's a tally)
* Flow vs stock
* Stop issuing bonds and OMF (overt monetary finance)
* Consolidated government (Treasury+Fed)
* Net financial assets vs Net worth (real assets)
* Bidding for a war and a bubble of ready-to-redeploy resources after a won war
* Fed effectively delegating currency issuing power to private Blackrock and covert government spending, bail-ins with obscured accountability that sidesteps the Treasury - nono
* Treasury bonds issue dance. Fed buying treasuries and putting them on its balance sheet is but a completion of government spending - money has already been spent and already contributed to inflation pressure before the QE. Bond issuance is a monetary operation - the one original contribution of MMT.
* Bond vigilantes myth - CB is calling the shots
* Money multiplier myth
* Central bank independence myth
* Job guarantee / employer of the last resort at minimum wage - "big government" options with significant corruption and arguably some inflation pressure risk (but potential net-positive social benefit) that don't have to be taken up.


Links to previous writings/attempts at formulation and examples:

* https://vivabarneslaw.locals.com/post/360191/article-of-the-day-tuesday-1-19-21?cid=1592494
* https://www.propertychat.com.au/community/threads/rba-quantitative-easing-what-exactly-is-it-how-does-it-work-in-australia.44850/#post-836135
* https://www.propertychat.com.au/community/threads/rba-quantitative-easing-what-exactly-is-it-how-does-it-work-in-australia.44850/#post-836165





-----
from Locals:

@RobertBarnes Sorry to see Robert doesn't actually understand MMT (hopefully, yet). The 2015 Primer book from Wray is a great intro into the concepts - should disperse the strawman for anyone open to learning more.

I personally think that understanding MMT could actually be THE thing that could bring the right and the left a lot closer together (without any side needing to give up its moral positions), allowing to instead focus the economics and wider political discussion to "how much spend where" instead of the current "our religion vs yours" war-like status quo.

A potential Populist Trump card no less.

January 19, 2021
@RobertBarnes, @krab The way I'm reading MMT is not that resources are idle, it's that the government usurps useful real production capacity for itself by way of levying taxes (which create demand for currency, which create demand for dollars issued to private sector through government spending, if your work it backwards).

The original contribution of MMT is that the act of government issuing bonds is a monetary policy operation, identical to a central bank selling a bond it previously purchased.
The effect of this is that interbank interest rate doesn't fall to its natural rate of 0%, as it drains excess reserves in the system. And the difference between Fed doing it and the Treasury doing it is that under current self-imposed constraints, the Treasury has an unlimited capacity to do so vs the Fed can only sell bonds that are already on its balance sheet.
Another effect is that big bank corporations can have a risk-free asset (sort of a savings account denominated in bonds) which pays them a non-zero interest rate for nothing - great subsidy for them :)
And these days the reserves don't even need to be drained anymore (i.e. bonds don't need to be sold to maintain non-zero interest rate) because central banks across the world, incl. recently the Fed, now pay a non-zero "floor interest rate" on banks' reserve deposits with the CB. The Treasury could just instruct the Fed (Treasury+Fed is what MMT calls "consolidated government" ) to spend/issue/"print" the money directly into the recipients' accounts without the unnecessary dance-around, and also remove the redeemed tax dollars from circulation, which would have exactly the same effect as the current convention (but would not fit the common story and "understanding" ).

January 19, 2021
@RobertBarnes The issue is that the post promotes a misleading commentary on what MMT actually is. Most people who comment on MMT do it from an incompat neolib/monetarist/Friedman view. MMT is like special relativity theory - its understanding requires a "Gestalt shift" (in a technical/theoretical way, not some yoga). When you said in the AMA you understood "partially" it was - with greatest respect - an indication it hasn't clicked yet. Your best bet is Wray's book, everything is in there, rest is just detail. Would be honored to jump on a call to give you an intro/answer questions.

Wray: Modern Money Theory. 2nd Ed. A Primer on Macroeconomics for Sovereign Monetary Systems"

Have a peek into the Introduction here:
https://books.google.com.au/books?id=pqGkCgAAQBAJ&pg=PT5&source=gbs_selected_pages&cad=2#v=onepage&q&f=false

Then buy here - 2nd edition:
https://www.amazon.com.au/Modern-Money-Theory-Macroeconomics-Sovereign/dp/1137539909

Also this blog article (from the paragraph that mentions "HPM" it's a sort of "MMT in a nutshell" ). Ignore Mitchell's leftie activist emotional language.
Mitchell: Money multiplier and other myths
http://bilbo.economicoutlook.net/blog/?p=1623

Next, in my opinion this is a great short 30pg paper, discussing the relationship of interest rates and government deficit spending, and also the likelihood of a CB "going rogue" before "government" would intervene:
Wray: Central Bank Independence: Myth and Misunderstanding
http://www.levyinstitute.org/pubs/wp_791.pdf

And lastly, a QE-specific blog post by an Australian economist Bill Mitchell, one of my favourite on the topic:
http://bilbo.economicoutlook.net/blog/?p=40250
Mitchell: Bank of Japan once again shows who calls the shots

Also check out the two videos here: https://vivabarneslaw.locals.com/post/352172/saturday-ask-me-anything-1-16-21?aid=712952

I don't foresee the MMT lens sinking into the heads of most people as its implications are too counterintuitive and even "immoral". 🤯

January 20, 2021
@RobertBarnes Just as an example re the premise in your OP: "Governments will be able to borrow and spend as much as they want ".

Incorrect: the word "borrow" is nonsensical under MMT. One cannot borrow their own IOU. - See Wray& Mitchell's 7 minute video from 2009 linked in your AMA comment thread I linked to above.
"As much as they want" - incorrect, their constraint is inflation/real production capacity.
During Covid governments pumped trillions into the economy but it didn't increase inflation because it just topped up the decrease in private spending and didn't increase aggregate demand at any given time. (Increased "stock" of reserves or bonds on Fed's balance sheet doesn't matter - it's just a tally of cumulative amount of past deficit spending, with no other meaning or effect. Bernanke is correct. It's the "flow" of money that matters and how it affects aggregate demand. If households lose jobs and stop spending and government steps in with helicopter money the net effect on inflation is zero.)

I also made a few more comments in this thread earlier today, attempting to analyse the bigger picture. https://vivabarneslaw.locals.com/post/360191/article-of-the-day-tuesday-1-19-21?aid=732584

January 20, 2021
@RichardKT @RobertBarnes MMT is a theory, a neutral lens that shows how things (in this case money) work, it's not a policy. So MMT can't "avoid inflation" or "maintain low interest rates".

It's like gravity - you need to be aware of it when you fly a plane, but it's you (or policy/government) that flies the plane - and you can crash it if you oversteer and don't check your altitude (spending) and pitch angle (inflation) often enough or you (Congress) can fly just fine forever if you're aware of your constraints. You can also weather large storm clouds (pandemic) by flying/spending higher for a while - saving lives and livelihoods, building infrastructure projects FDR-style etc. There is no absolute limit on size of annual spending (and cumulative deficit is even less important) - government (sth like "Federal inflation commission" ) needs to measure inflation ex post in real economy and make spending adjustments based on the empirical data. The book above goes into details of possible legally-mandated autocorrection mechanism, largely inspired by 1930s "functional finance" of Abba Lerner.

This is one of longer videos in a series that gives deeper insight and shows some important diagrams (the two completely separate money/dollar systems - CB+Treasury and asset side of commercial banks using reserve dollars, and liability side of commercial banks and private sector using privately created "bank dollars"/IOUs that are promised to be convertible to reserve dollars)

https://m.youtube.com/watch?v=WfF4peuP6EI
There's an "Aula 2" video in the series that goes into the detail of how the "endogenous" money creation in the private system works and its relationship to the reserve system.

January 20, 2021
@RichardKT @RobertBarnes Central bankers and mainstream economists for a while believed (and most still believe) that there's a ideal natural unemployment rate that leads to a "healthy noninflationary economy",NAIRU (non accelerating inflation rate of unemployment),at the cost of some people being out of jobs. They first thought it was around 7% then 6%, 5% and now (pre-covid) we have almost full employment and hardly any CPI infl. MMT scholars say NAIRU is a myth and that the inflation just has to be measured as you go and adjust gov spending based on that in a short-ish feedback loop (say 1 year).
Look at Japan: decades of "crazy" deficits, BoJ now owns 45% or so of all bonds ever issued, yet no inflation despite trying as hard as they can. Japan is the MMT lab that keeps proving orthodox economists (and silly bond traders who bet against BoJ - the so called "widowmaker trade" ) wrong over and over, and shows that attempting to plan inflation is like trying to get a 5 year plan right in the Soviet Union :)

It's crucial to remember this all only applies to sovereign federal finances (UK, US, Australia) - state and local finances, and silly countries who gave up their currency issuing and therefore fiscal sovereignty - operate under the balanced budget/household finance analogy constraint (California, Italy, Greece) because they don't have the currency-issuing power... there taxes actually do pay for the local spending... but the Feds or ECB can still bail them out as they often do if they beg nicely enough.

So to answer your question "what's the limit". Impossible to know, same as with NAIRU, it could be -$10T/year in a pandemic year when aggregate demand fell of the cliff, it could be +$1T surplus in a year when you need to slow down in a hot part of the cycle. It could be anything. And if there's a political movement that's serious about this they need sell the automatic stabilisers idea to Congress. It's not dissimilar to already existing independence of FED.

January 20, 2021
@krab You are partially right, Austrian economics perspective still applies to local and state government spending (as these are currency-users, not currency-issuers). However since 1971 it doesn't to federal spending. This is what Austrians don't understand. Other than wanting to go back to the "good old days", they missed the fiat paradigm shift on the federal level and its positive implications/options.
This is from a guy who was Austrian from 2007-2019, comes from a place the theory started, and still has great respect for Austrian scholars and proponents.
Newton still applies on some level, but Einstein also applies in some circumstances and these could be leveraged for greater good - as we're seeing with all the pandemic relief helicopter money e.g. in the UK and Australia, and now US. The risk is corruption and too much government power of course, but that's a political issue, not a MMT/gravity issue.

January 20, 2021
@krab I disagree that we disagree :) There's no free lunch, but the limit is real production capacity that can be bought. The unit of account doesn't matter. Austrians associate inflation (true to the word "inflate", like a bubble) with growing monetary base (reserves and physical coins and notes). MMT and Bernanke say that's incorrect. It's the govt spending that drives inflation, as the new gov't spending competes for real resources with other bidders.

That's really the only difference imho: Austrians say "you gotta have a strictly balanced budget" (but where did the first ever dollar that was ever used to pay the first tax came from? hint: metalism story is apparently a myth), MMT proponents say "post-1971, a balanced budget is a historical dogma, a now-artificial constraint, instead we should measure inflation as the real constraint of spending" ("and stop issuing bonds - avoiding needing to issue further currency for the interest payments, creating inflation" ). But then you also might need to tell people the truth that their taxes don't pay for government spending and change-manage that.

You can literally have the exact same fiscal policy "with MMT" as in "pre MMT" if that's the political agreement.
However 2020 is a great demonstration that MMT insights are already fully being implemented, even if it's not being admitted. Almost all the money being issued by treasuries (as bonds) is being mopped up by central banks and forever-parked on their balance sheets (exactly like in 2008). And nothing will happen, it will just sit there forever, while Bitcoin and gold will correct again or stagnate (let's check in 2 years). They could avoid the hassle of issuing bonds and instead instruct the central bank to directly deposit to the welfare etc. recipients' accounts, for the exact same end result.

P.S. I corrected a mistake in a previous post where I initially referred to NAIRU as if it was an interest rate rather than unemployment rate, apologies for that blunder.

January 20, 2021
@krab But if you want to build public infrastructure (roads, pay school teachers) as a policy goal, you have no choice but to bid some resources away from the private sector. The tension will always be there and "create bubbles" (e.g. bubble of soldiers after a successfully won WW2 and now they'd be better utilised in a private sector when need for these resources in the gov sector disappeared).

Yes, I should have said monetary base "in the discussion related to quantitative easing and increasing Fed's balance sheets/"Fed printing new reserves" " which is where this mostly comes up.
Didn't realize you might be thinking in the broader context of private banks' licence to create private credit in the 2nd, private system. However, there are no new net financial assets created in that as the new private money (deposit, private bank's liability) is always offset (and later extinguished) by the corresponding asset on the left side of the private bank's balance sheet (the loan repayment promise/mortgage you signed). It's zero-sum in the private sector from the point of view of double entry accounting. That's what most people don't get. They conflate the reserve system (monetary base) with the "private dollars" system, because the unit of account is called the same and because of the promised convertibility.

January 20, 2021
@krab There are financial assets created, but then they are destroyed as the debt is repaid over time, i.e. over the lifetime of the loan there are no new NET financial assets created. New NFAs only come through government spending (i.e. new fiat reserves) that is then not fully withdrawn out of the system by taxation. [Correction: removed errorenous sentence that said that these NFAs only exist on the asset side of the private banks' balance sheet...of course there is a corresponding deposit on the liability side, equalling amount of new reserves]

MMT doesn't try to "remove the limit", it just exposes that it's a self-imposed, if you will ideological, political constraint and it neutrally shows how the system actually works. It explains gravity, but doesn't tell you how to fly your plane - that's the theory part of MMT.
But of course this often leads to a political discussion about what these newly discovered truths could mean for policy, but that's not part of MMT the theory. And moreover, it's so counterintuitive and often seen as "immoral" that it's unlikely to be teachable to the wider population and getting wide-enough political support for. Columbus and Galieo could talk about this at length :)

So the application of it might need to be continued to be deferred to the unelected central bankers and have them continue keep doing it as their black box "woodoo" (as they've been correctly doing for the past decade and a half) and both political sides can keep complaining and warring about it in perpetuity from their respective uneducated/reduced ideological/moral standpoints, maintaining the division. Or not?

What's interesting is that Bannon is now calling on Sanders supporters to join the populist movement with the Trumpists. MMT and its understanding could be the unifying element that could get that done (with, paradoxically, the conservative side probably having a bigger educational hurdle to jump in unlearning some deep rooted assumptions). @RobertBarnes

January 21, 2021
@RobertBarnes, @krab One more point: When the Fed starts buying private debt (like in the 2020 deal with Blackrock), that's the scary crossing of the Rubicon where money/loans created in the private banking system crosses into the reserves system.
In other words, it's covert government spending, by unelected gov officials, outside of the oversight of the Treasury, outside of the conventional proxying by way of issuing bonds. And without apparent oversight by the Congress, although Congress is ultimately responsible (even if they'd want to make you believe they aren't due to Fed's supposed "independence" - see paper above; remember also Bernanke's "Fed is a creature of the Congress" statement).
That's when private losses are bailed out seemingly without any accountability or political/people's explicit agreement.
It's these types spending (regardless of whether at year's end it adds up to being a "deficit" or "surplus" year), rather than deficit spending per se that conservatives/Austrian-minded/inflation-concerned people should imho be concerned about (and drive policy change on, away from such behind-closed-doors "voodoo deals", what essentially is a delegation/privatization of the federal currency-issuing superpower to bureucrats and then re-delegation again from them to private hands of Blackrock&friends putting the orders in for themselves).

This thread appears to have gotten a honorable mention at 1:01:33 😹 https://m.youtube.com/watch?v=v2JrEIdgpw4&t=1h1m33s


January 22, 2021
@RobertBarnes, @krab The purchase of government treasuries ("debt" ) during the primary issue is settled, and can only be settled (same as tax bills) by using reserves or physical cash (let's call this money $USDR).
$USDR reserves, MMT says (do you agree?) can only come into existence through the central bank issuing this money: by way of QE, "debt monetisation" or OMF (overt monetary finance - when the gov chooses to skip the unnecessary intermediate operation of issuing bonds first and instead just instructs the CB to mark up the spending recipient's bank's $USDR reserve account directly + at the same time asks the private bank to make the corresponding private deposit on the liability side in $USDP, "dollars promised/private" ).

$USDRs that come back to the issuer (fed. government) through tax revenue can either be reused, or burned&reissued (by keystrokes) - makes no difference, and that's why imho it would be more correct to say MMT "Mutated My Taxes" (understanding thereof) rather than "Magic Money Tree".

Deep in every Austrian is a strong desire to pay less tax and we think the policy to achieve that is small government. That is correct. Reduced government spending removes inflationary pressure and reduces the amount of taxation needed to counteract the inflationary pressure.
What MMT the theory brings to the table, in my opinion, is a real prospect that with an Austrian-philosophy policy of austere small government, you could actually abolish personal income tax for say the bottom 80-90% of the population altogether, while not introducing higher CPI inflation (say keeping it at 2% growth) if you could convince your Congressional reps to fly the plane low enough (but probably not too far off from where we're flying already in normal years, ex Covid spending), Japan is a good precedent.

Maybe the prospect of abolishing personal income tax is what could whet the appetite of Austrians enough to dive into the literature with greater curiosity and seriousness.

January 22, 2021
@RobertBarnes, @krab This thread is getting ridiculous 🤪 Maybe I should just write a book/paper titled
MMT for Austrians, Republicans, Chicagoers and dummies
and start it like this:

Thanks to Robert Barnes, a great lawyer and defender of regular people and the Constitution, for inciting the discussion that lead to this book. He should be impeached accordingly.

Thank you to Randall Wray, Bill Mitchell and Warren Mosler, the trinity of the minds that charted these new - yet old - important maps. Don't let these genious detectives get away with stripping the emperor in our heads naked.

???
😂

Do you think it might be worth a Kickstarter campaign or something, or am I still way off influencing even crustaceans? (no offence @krab ! 😉)
😅

January 24, 2021
@krab (cc @RobertBarnes @Jaybone86 @superfly00000 @Reno7782 @RichardKT )
I typed up a few introductory paragraphs. Would greatly appreciate @krab if you were willing to take a little bit of your time to read about a page's worth of text (until the divider "TBC" ) and let me know if there's anything up to that point in that model that you disagree with. I believe there shouldn't be :) Of course would appreciate any other comments and suggestions.
Have an Austrian money genesis model until the point where a Federal Reserve/federal paper money is introduced.

Thank you!

Link: https://bitbucket.org/borhub/mmt-for-austrians-dummies-etc/src/master/README.md

P.S. Did I go overboard with the faith dedications? I thought it might help appeal to conservatives, but might be counterproductive. Pls let me know if you think that it might be potentially undermining the credibility with the intended audience.

January 24, 2021
@RobertBarnes, @krab Thanks. Great to hear you agree with my model interpretation up to TBC so far.
I think you'd be the perfect reviewer since you've read great amounts of the Austrian literature.

I just added a section on why Fed's quantitative easing or other asset purchases don't add to the money supply. I'm calling the concept "latent reserves" and conversion of "private dollar deposits" (issued through the private banks on the private side of the system) to "reserve dollar deposits" by Fed - this effectively creating identity between private dollars and reserve dollars.
This part is a super dirty draft but I think the logic stands.
https://bitbucket.org/borhub/mmt-for-austrians-dummies-etc/src/master/README.md

If you're interested you can also read my earlier step-by step walkthrough of Coronavirus relief spending through bond issuance (and the unnecessity of that) and the almost immediate monetization onto central banks' balance sheets, in the Propertychat links at the bottom of the updated file.

Do you have any links to where you think Murphy dealt with MMT well? I'd love to see if I can poke some holes into it.

Feb 18, 2021
@RobertBarnes, @krab Hi Krab, I added some more content to my "paper". A few more paragraphs on the creation of Fed and fiat money on the private bank side and then later on the Fed side (1971). Didn't really get deep into MMT yet, but on the cusp - which should make the "story" up to this point hopefully still agreeable with Austrians, but more evolved than last time.
Also included excerpts from an email where I consolidate the Austrian argument that "government needs to get money from the private system first" with MMT which traditionally says that "government has to spend first" but which is not necessarily the case in the real world that either evolved from a non-fiat state, or the Fed makes the initial reserves funding through nationalisation of some previously private credit (the credit asset [the dollar deposit] + the liability [the loan obligation]). The monetary system creation can happen both ways, both the Austrian way as well as "pure fiat MMT" way.
MMTers don't understand the Austrian history/genesis of the system and Austrians don't understand the new world. Keen to hear if you have any comments :) 